interface IUser {
  username: string;
  uuid: string;
}

type Users = Map<string, IUser>;
