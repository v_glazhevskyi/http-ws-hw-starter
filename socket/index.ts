import * as config from './config';
import { Server, Socket } from 'socket.io';
import authService from '../services/authService';

export default (io: Server) => {
  io.on('connection', (socket: Socket) => {
    const { username, uuid } = socket.handshake.query;
    try {
      const user = authService.login({ uuid, username });
      io.to(socket.id)
        .emit('auth:success', user);
    } catch (error) {
      console.error(error);
      io.to(socket.id)
        .emit('auth:error:exist-username', error.message);
    }
  });
};
