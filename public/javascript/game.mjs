const username = sessionStorage.getItem('username');
const uuid = sessionStorage.getItem('uuid');

if (!username) {
  window.location.replace('/login');
}

console.log({ username, uuid });

const socket = io('', { query: { username, uuid } });

socket.on('auth:success', (user) => {
  sessionStorage.setItem('uuid', user.uuid);
});

socket.on('auth:error:exist-username', (message) => {
  window.alert(message);
  sessionStorage.clear();
  window.location.replace('/login');
});
