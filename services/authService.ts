import store from '../store';
import { v4 as uuidv4 } from 'uuid';

class AuthService {
  login(user: IUser) {
    const { username, uuid } = user;
    const existUser = store.users.get(username);

    if (!existUser) {
      const newUser = { username, uuid: uuidv4() };
      store.users.set(username, newUser);
      return newUser;
    }

    if (existUser.uuid === uuid) {
      return existUser;
    }

    throw new Error('This username already used. Try another.');
  }
}

export default new AuthService();
